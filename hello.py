from flask import Flask, request
from markupsafe import escape

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Welcome!'

@app.route('/hello/', methods=['POST', 'GET'])
def hello():
    try:
        if request.method == 'POST':
            searchword = request.args.get('day', '')
            _sendToAPI = request.args.get('sendToAPI')
            return f"Today its: {searchword} | {_sendToAPI}"
    
    except KeyError as e:
        return f"Error details (KeyError): {str(e)}"

    except AttributeError as e:
        return f"Error details (AttributeError): {str(e)}"
    
    return 'Welcome'

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % escape(username)

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /path/
    return 'Subpath %s' % escape(subpath)